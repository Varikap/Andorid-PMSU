package com.example.w10.pmsu.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.w10.pmsu.R;
import com.example.w10.pmsu.adapters.CommentsAdapter;
import com.example.w10.pmsu.service.CommentService;
import com.example.w10.pmsu.service.ServiceUtils;
import com.example.w10.pmsu.service.UserService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.example.w10.pmsu.model.Comment;
import com.example.w10.pmsu.model.Post;
import com.example.w10.pmsu.model.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsFragment extends Fragment {
    View view;

    Comment c1 = new Comment();
    User u1 = new User();
    Comment c2 = new Comment();
    User u2 = new User();
    Comment c3 = new Comment();
    User u3 = new User();
    List<Comment> comments = new ArrayList<>();
    private CommentsAdapter commentsAdapter;
    private SharedPreferences sharedPreferences;
    private CommentService commentService;
    private UserService userService;
    private Post post;
    private static User user;

    private RadioButton btn_like;
    private RadioButton btn_dislike;

    private EditText write_comment_title;
    private EditText write_comment;
    private ListView listView;

    public CommentsFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.comments_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String jsonMyObject = null;
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            jsonMyObject = extras.getString("Post");
        }

        post = new Gson().fromJson(jsonMyObject, Post.class);
        listView = view.findViewById(R.id.comments_list);

        commentService = ServiceUtils.commentService;
        Call<List<Comment>> call = commentService.getCommentsByPost(post.getId());

        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                comments = response.body();
                commentsAdapter = new CommentsAdapter(getContext(), comments);

                listView.setAdapter(commentsAdapter);
                consultPreferences();
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {

            }
        });

        sharedPreferences = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        final String ulogovani = sharedPreferences.getString("User", "");

        userService = ServiceUtils.userService;

        Call<User> callUser = userService.getUserByUsername(ulogovani);

        callUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                user = response.body();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        Button post_comment = view.findViewById(R.id.comment_post_btn);
        write_comment_title = view.findViewById(R.id.write_comment_title);
        write_comment = view.findViewById(R.id.write_comment);


        post_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Comment comment = new Comment();

                String comment_title = write_comment_title.getText().toString();
                String txtComment = write_comment.getText().toString();

                comment.setTitle(comment_title);
                comment.setDescription(txtComment);
                Date date = Calendar.getInstance().getTime();
                comment.setDate(date);
                comment.setAuthor(user);
                comment.setPost(post);
                comment.setLikes(0);
                comment.setDislikes(0);


                Call<ResponseBody> call = commentService.addComment(comment);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(getContext(), "Added comment", Toast.LENGTH_SHORT).show();

                        commentsAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void consultPreferences(){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String sortComments = sharedPreferences.getString("pref_sortComments", "");

        if (sortComments.equals("oldest")){
            commentsByDate();
        }

        if (sortComments.equals("newest")){
            commentsByDateOld();
        }

        if (sortComments.equals("rating")){
            commentsByRating();
        }

        commentsAdapter.notifyDataSetChanged();
    }

    private void commentsByDate(){
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment com, Comment t1) {
                return com.getDate().compareTo(t1.getDate());
            }
        });
        commentsAdapter.notifyDataSetChanged();
    }

    private void commentsByDateOld(){
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment com, Comment t1) {
                return t1.getDate().compareTo(com.getDate());
            }
        });
        commentsAdapter.notifyDataSetChanged();
    }

    public void commentsByRating(){
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment com, Comment t1) {
                return Integer.valueOf(t1.getLikes()).compareTo(com.getLikes());
            }
        });
        commentsAdapter.notifyDataSetChanged();
    }

}
